//
//  URLQuerryItemEncoder.swift
//  HadiMovies
//
//  Created by Hadi KUDSI on 18/09/2023.
//

import Foundation

enum URLQueryItemEncoder {
    static func encode<T: Encodable>(_ encodable: T) throws -> [URLQueryItem] {
        let parameterData = try JSONEncoder().encode(encodable)
        let parameters = try JSONDecoder().decode([String: HTTPParameter].self, from: parameterData)
        let parametersFiltered = parameters.filter { $1.description != "" }
        return parametersFiltered.map { URLQueryItem(name: $0, value: $1.description) }
    }
}
