//
//  APIError.swift
//  HadiMovies
//
//  Created by Hadi KUDSI on 18/09/2023.
//

import Foundation

public enum APIError: Error {
    case encoding
    case decoding
    case server(message: String)
}
