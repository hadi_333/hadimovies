//
//  APIClient.swift
//  HadiMovies
//
//  Created by Hadi KUDSI on 18/09/2023.
//

import Foundation

protocol APIClientProtocol {
    func send<T: APIRequest>(_ request: T, completion: @escaping (Result<T.Response, Error>) -> Void)
}

class APIClient: APIClientProtocol {
    private let baseEndpointUrl: URL?
    private let session: URLSession

    init() {
        if let urlstring = Plist.getUrlFromPlist(),
            let url = URL(string: urlstring) {
            baseEndpointUrl = url
        } else {
            baseEndpointUrl = nil
        }
        session = URLSession(configuration: .default)
    }

    func send<T: APIRequest>(_ request: T, completion: @escaping (Result<T.Response, Error>) -> Void) {
        if let endpoint = self.endpoint(for: request) {
            let task = session.dataTask(with: endpoint) { data, _, error in
                if let data = data {
                    do {
                        let moviesResponse = try JSONDecoder().decode(T.Response.self, from: data)
                            completion(.success(moviesResponse))
                    } catch {
                        print(String(describing: error))
                        completion(.failure(error))
                    }
                } else if let error = error {
                    print(String(describing: error))
                    completion(.failure(error))
                }
            }
            task.resume()
        }
    }

    private func endpoint<T: APIRequest>(for request: T) -> URLRequest? {
        guard let baseUrl = URL(string: request.resourceName, relativeTo: baseEndpointUrl) else {
                   print("Bad resourceName: \(request.resourceName)")
                   return nil
        }

        guard var components = URLComponents(url: baseUrl, resolvingAgainstBaseURL: true) else { return nil }

        let customQueryItems: [URLQueryItem]

        do {
            customQueryItems = try URLQueryItemEncoder.encode(request)
        } catch {
            fatalError("Wrong parameters: \(error)")
        }
        components.queryItems = customQueryItems
        
        var urlRequest = URLRequest(url: baseUrl)
            
        urlRequest.addValue("Bearer " + Constants.apiKey, forHTTPHeaderField: "Authorization")
        
        urlRequest.url = components.url
        return urlRequest
    }
}
