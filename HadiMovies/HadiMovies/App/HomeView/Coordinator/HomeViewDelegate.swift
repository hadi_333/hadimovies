//
//  HomeViewDelegate.swift
//  HadiMovies
//
//  Created by isima on 19/09/2023.
//

import Foundation

protocol HomeViewDelegate: AnyObject {
    func didSelectCell(detailId: Int)
}
