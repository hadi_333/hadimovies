//
//  MoviesMapper.swift
//  HadiMovies
//
//  Created by isima on 19/09/2023.
//

import Foundation

final class MoviesMapper {

    static func moviesEntityToMoviesModel(moviesEntity: MoviesEntity) -> [MovieModel] {
        if let movieEntity = moviesEntity.results {
            return movieEntity.map { MovieModel(movieEntity: $0)}
        }
        return []
    }
}
