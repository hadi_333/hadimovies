//
//  MoviesRepository.swift
//  HadiMovies
//
//  Created by Hadi KUDSI on 18/09/2023.
//

import Foundation

protocol MoviesRepository {
    func getPopularMovies(_ completion: @escaping (([MovieModel]) -> Void))
    func getNowPlayingMovies(_ completion: @escaping (([MovieModel]) -> Void))
    func getTopRatedMovies(_ completion: @escaping (([MovieModel]) -> Void))
    func getUpcomingMovies(_ completion: @escaping (([MovieModel]) -> Void))
}

final class MoviesRepositoryImpl: MoviesRepository {
    
    let apiClient: APIClientProtocol
    
    init(apiClient: APIClientProtocol = APIClient()) {
        self.apiClient = apiClient
    }
    
    func getPopularMovies(_ completion: @escaping (([MovieModel]) -> Void)) {
        apiClient.send(GetPopularMovies()) { result in
            switch result {
            case .success(let data):
                let movies = MoviesMapper.moviesEntityToMoviesModel(moviesEntity: data)
                completion(movies)
            case .failure(let error):
                debugPrint(error.localizedDescription)
            }
        }
    }
    
    func getNowPlayingMovies(_ completion: @escaping (([MovieModel]) -> Void)) {
        apiClient.send(GetNowPlayingMovies()) { result in
            switch result {
            case .success(let data):
                let movies = MoviesMapper.moviesEntityToMoviesModel(moviesEntity: data)
                completion(movies)
            case .failure(let error):
                debugPrint(error.localizedDescription)
            }
        }

    }
    
    func getTopRatedMovies(_ completion: @escaping (([MovieModel]) -> Void)) {
        apiClient.send(GetTopRatedMovies()) { result in
            switch result {
            case .success(let data):
                let movies = MoviesMapper.moviesEntityToMoviesModel(moviesEntity: data)
                completion(movies)
            case .failure(let error):
                debugPrint(error.localizedDescription)
            }
        }

    }
    
    func getUpcomingMovies(_ completion: @escaping (([MovieModel]) -> Void)) {
        apiClient.send(GetUpcommingMovies()) { result in
            switch result {
            case .success(let data):
                let movies = MoviesMapper.moviesEntityToMoviesModel(moviesEntity: data)
                completion(movies)
            case .failure(let error):
                debugPrint(error.localizedDescription)
            }
        }

    }
}
