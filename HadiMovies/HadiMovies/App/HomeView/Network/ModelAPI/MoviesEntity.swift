//
//  MoviesEntity.swift
//  HadiMovies
//
//  Created by Hadi KUDSI on 18/09/2023.
//

import Foundation

// MARK: - MoviesEntity

struct MoviesEntity: Codable {
    let page: Int?
    let results: [MovieEntity]?
    let totalPages: Int?
    let totalResults: Int?

    enum CodingKeys: String, CodingKey {
        case page
        case results
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
}
