//
//  GetNowPlayingMovies.swift
//  HadiMovies
//
//  Created by isima on 19/09/2023.
//

import Foundation

struct GetNowPlayingMovies: APIRequest {
    typealias Response = MoviesEntity
    
    var resourceName: String {
        return "/" + Constants.apiVersionLatest + "/movie/now_playing"
    }
    
    private let language: String
    private let page: String
    
    init(language: String = "en-US", page: String = "1") {
        self.language = language
        self.page = page
    }
}
