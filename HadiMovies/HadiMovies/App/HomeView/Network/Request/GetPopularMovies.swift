//
//  GetPopularMovies.swift
//  HadiMovies
//
//  Created by Hadi KUDSI on 18/09/2023.
//

import Foundation

struct GetPopularMovies: APIRequest {
    typealias Response = MoviesEntity
    
    var resourceName: String {
        return "/" + Constants.apiVersionLatest + "/movie/popular"
    }
    
    private let language: String
    private let page: String
    
    init(language: String = "en-US", page: String = "1") {
        self.language = language
        self.page = page
    }
}
