//
//  MoviesModel.swift
//  HadiMovies
//
//  Created by isima on 19/09/2023.
//

import Foundation

// MARK: - MovieModel

struct MovieModel {
    let id: Int
    let title: String
    let releaseDate: String
    let voteAvg: Double
    let image: String
    
    init(movieEntity: MovieEntity) {
        self.id = movieEntity.id
        self.title = movieEntity.title ?? "unknown"
        self.releaseDate = movieEntity.releaseDate ?? "xx-xx-xxxx"
        self.voteAvg = movieEntity.voteAverage ?? 0.0
        self.image = movieEntity.posterPath ?? ""
    }
}
