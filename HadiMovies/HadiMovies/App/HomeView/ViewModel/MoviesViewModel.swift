//
//  MoviesViewModel.swift
//  HadiMovies
//
//  Created by isima on 19/09/2023.
//

import Foundation

protocol MoviesViewModel {
    func fetchPopularMovies()
}

class MoviesViewModelImpl: MoviesViewModel {
    private let moviesRepository: MoviesRepository
    
    let popularMovies:DynamicValue<[MovieModel]>
    let nowPlayingMovies:DynamicValue<[MovieModel]>
    let topRatedMovies:DynamicValue<[MovieModel]>
    let UpcomingMovies:DynamicValue<[MovieModel]>
    let sectionTitles: [String] = Constants.homepageTypes
    
    init(moviesRepository: MoviesRepository = MoviesRepositoryImpl()) {
        self.moviesRepository = moviesRepository
        self.popularMovies = DynamicValue<[MovieModel]>([])
        self.nowPlayingMovies = DynamicValue<[MovieModel]>([])
        self.topRatedMovies = DynamicValue<[MovieModel]>([])
        self.UpcomingMovies = DynamicValue<[MovieModel]>([])
        fetchHomePage()
    }
    
    func fetchHomePage() {
        fetchPopularMovies()
        fetchNowPlayingMovies()
        fetchTopRatedMovies()
        fetchUpcomingMovies()
    }
    
    func fetchPopularMovies() {
        moviesRepository.getPopularMovies { [weak self] movies in
            guard let self = self else {return}
            self.popularMovies.value = movies
                        
        }
    }
    
    func fetchNowPlayingMovies() {
        moviesRepository.getNowPlayingMovies { [weak self] movies in
            guard let self = self else {return}
            self.nowPlayingMovies.value = movies

        }
    }

    func fetchTopRatedMovies() {
        moviesRepository.getTopRatedMovies { [weak self] movies in
            guard let self = self else {return}
            self.topRatedMovies.value = movies

        }
    }

    func fetchUpcomingMovies() {
        moviesRepository.getUpcomingMovies { [weak self] movies in
            guard let self = self else {return}
            self.UpcomingMovies.value = movies

        }
    }
}
