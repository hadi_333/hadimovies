//
//  CollectionViewCell.swift
//  HadiMovies
//
//  Created by Hadi KUDSI on 18/09/2023.
//

import UIKit

class CollectionViewCell: UITableViewCell {
    weak var delegate: HomeViewDelegate?

    static let identifier = "CollectionViewCell"
    
    var viewModel: MoviesViewModelImpl?
    var section: Int?
    
    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 150, height: 240)
        layout.scrollDirection = .horizontal
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.register(MoviesCollectionCell.self,
                            forCellWithReuseIdentifier: MoviesCollectionCell.identifier)
        collection.backgroundColor = UIColor(resource: R.color.backGroundColor)
        return collection
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = UIColor(resource: R.color.textColor)
        contentView.addSubview(collectionView)
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        collectionView.frame = contentView.bounds
    }
    
    public func configure(viewModel: MoviesViewModelImpl, section: Int) {
        self.viewModel = viewModel
        self.section = section
        self.collectionView.reloadData()
    }
}

extension CollectionViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MoviesCollectionCell.identifier, for: indexPath) as? MoviesCollectionCell else {return UICollectionViewCell()}
        
        if section == 0 {
            if let movie = viewModel?.nowPlayingMovies.value {
                cell.configure(image: Constants.imagePrefixe +
                               (movie[indexPath.row].image),
                               title: movie[indexPath.row].title,
                               subtitle: movie[indexPath.row].releaseDate)
            }
        }
        if section == 1 {
            if let movie = viewModel?.popularMovies.value {
                cell.configure(image: Constants.imagePrefixe +
                               (movie[indexPath.row].image),
                               title: movie[indexPath.row].title,
                               subtitle: movie[indexPath.row].releaseDate)
            }
        }
        if section == 2 {
            if let movie = viewModel?.topRatedMovies.value {
                cell.configure(image: Constants.imagePrefixe +
                               (movie[indexPath.row].image),
                               title: movie[indexPath.row].title,
                               subtitle: movie[indexPath.row].releaseDate)
            }
        }
        if section == 3 {
            if let movie = viewModel?.UpcomingMovies.value {
                cell.configure(image: Constants.imagePrefixe +
                               (movie[indexPath.row].image),
                               title: movie[indexPath.row].title,
                               subtitle: movie[indexPath.row].releaseDate)
            }
        }
    
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let popular = viewModel?.popularMovies.value,
           let top = viewModel?.topRatedMovies.value,
           let now = viewModel?.nowPlayingMovies.value,
           let upcoming = viewModel?.UpcomingMovies.value {
            return min(popular.count, top.count, now.count, upcoming.count)
        }
        return  0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if section == 0 {
            if let detailId = viewModel?.nowPlayingMovies.value[indexPath.row].id {
                delegate?.didSelectCell(detailId: detailId)
            }
        }
        if section == 1 {
            if let detailId = viewModel?.popularMovies.value[indexPath.row].id {
                delegate?.didSelectCell(detailId: detailId)
            }
        }
        if section == 2 {
            if let detailId = viewModel?.topRatedMovies.value[indexPath.row].id {
                delegate?.didSelectCell(detailId: detailId)
            }
        }
        if section == 3 {
            if let detailId = viewModel?.UpcomingMovies.value[indexPath.row].id {
                delegate?.didSelectCell(detailId: detailId)
            }
        }

    }
}
