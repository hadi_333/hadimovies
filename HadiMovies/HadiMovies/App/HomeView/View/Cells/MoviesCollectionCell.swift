//
//  MoviesCollectionCell.swift
//  HadiMovies
//
//  Created by Hadi KUDSI on 18/09/2023.
//

import UIKit
import Kingfisher

class MoviesCollectionCell: UICollectionViewCell {
    
    static let identifier = "MoviesCollectionCell"
    
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 8
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = UIColor.gray.cgColor 
        return imageView
    }()
    
    private let titleText: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = UIColor(resource: R.color.textColor)
        return label
    }()
    
    private let subtitleText: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = .gray
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentView.addSubview(imageView)
        self.contentView.addSubview(titleText)
        self.contentView.addSubview(subtitleText)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.imageView.frame = CGRect(x: 0,
                                      y: 0,
                                      width: self.contentView.bounds.width,
                                      height: self.contentView.bounds.height * 0.8)
        
        self.titleText.frame = CGRect(x: 10,
                                      y: self.imageView.frame.maxY,
                                      width: self.contentView.bounds.width + 5,
                                      height: self.contentView.bounds.height * 0.1)
        
        self.subtitleText.frame = CGRect(x: 10,
                                         y: self.titleText.frame.maxY,
                                         width: self.contentView.bounds.width + 5,
                                         height: self.contentView.bounds.height * 0.1)
    }
    
    public func configure(image: String, title: String, subtitle: String) {
        guard let url = URL(string: image) else {return}
        self.imageView.kf.setImage(with: url)
        self.titleText.text = title
        self.subtitleText.text = subtitle
    }
}
