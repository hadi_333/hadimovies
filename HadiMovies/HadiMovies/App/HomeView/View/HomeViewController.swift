//
//  HomeViewController.swift
//  HadiMovies
//
//  Created by Hadi KUDSI on 18/09/2023.
//

import UIKit

class HomeViewController: UIViewController, HomeViewDelegate {
    
    var viewModel: MoviesViewModelImpl?
        
    private let homeTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.register(CollectionViewCell.self,
                           forCellReuseIdentifier: CollectionViewCell.identifier)
        return tableView
    }()
    
    override func viewDidLoad() {
        self.viewModel = MoviesViewModelImpl()
        super.viewDidLoad()
        setUpView()
        setUpBinding()
    }
    
    private func setUpView() {
        view.backgroundColor = UIColor(resource: R.color.backGroundColor)
        view.addSubview(homeTableView)
        
        homeTableView.delegate = self
        homeTableView.dataSource = self
    }
    
    private func setUpBinding() {
        if let viewModel = viewModel {
            viewModel.popularMovies.addAndNotify(observer: self) { [weak self] movies in
                DispatchQueue.main.async {
                    self?.homeTableView.reloadData()
                }
            }
            viewModel.nowPlayingMovies.addAndNotify(observer: self) { [weak self] movies in
                DispatchQueue.main.async {
                    self?.homeTableView.reloadData()
                }
            }
            viewModel.topRatedMovies.addAndNotify(observer: self) { [weak self] movies in
                DispatchQueue.main.async {
                    self?.homeTableView.reloadData()
                }
            }
            viewModel.UpcomingMovies.addAndNotify(observer: self) { [weak self] movies in
                DispatchQueue.main.async {
                    self?.homeTableView.reloadData()
                }
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        homeTableView.frame = view.bounds
    }
    
    func didSelectCell(detailId: Int) {
        let detailsVC = DetailsViewController()
        detailsVC.detailId = detailId
        
        navigationController?.pushViewController(detailsVC, animated: true)
    }
    
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.sectionTitles.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else {return}
        header.textLabel?.font = .systemFont(ofSize: 15, weight: .bold)
        header.textLabel?.textAlignment = .center 
        header.textLabel?.textColor = UIColor(resource: R.color.textColor)
        header.textLabel?.text = header.textLabel?.text?.capitalized
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel?.sectionTitles[section]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: CollectionViewCell.identifier,
            for: indexPath) as? CollectionViewCell else { return UITableViewCell() }
        if let viewModel = viewModel {
            cell.configure(viewModel: viewModel, section: indexPath.section)

            cell.delegate = self
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
}
