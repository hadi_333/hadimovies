//
//  DetailsViewModel.swift
//  HadiMovies
//
//  Created by isima on 19/09/2023.
//

import Foundation

protocol DetailsViewModel {
    func fetchDetails()
}

class DetailsViewModelImpl: DetailsViewModel {
    
    private let detailsRepository: DetailsRepository
    let details:DynamicValue<(DetailsModel)>
    let id: String
    
    init(detailsRepository: DetailsRepository = DetailsRepositoryImpl(), id: Int) {
        self.detailsRepository = detailsRepository
        self.details = DynamicValue<DetailsModel>(DetailsModel())
        self.id = String(id)
        fetchDetails()
    }
    
    func fetchDetails() {
        detailsRepository.fetchDetails(id: self.id) { [weak self] details in
            guard let self = self else {return}
            self.details.value = details
            
        }
    }
}
