//
//  DetailsViewController.swift
//  HadiMovies
//
//  Created by isima on 19/09/2023.
//

import UIKit
import Kingfisher

class DetailsViewController: UIViewController {
    
    var detailId: Int?
    var viewModel: DetailsViewModelImpl?
    
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func viewDidLoad() {
        self.viewModel = DetailsViewModelImpl(id: detailId ?? 0)
        super.viewDidLoad()
        setUpView()
        setUpLayout()
        setUpBinding()
    }
    
    private func setUpView() {
        view.backgroundColor = .white
        
        view.addSubview(imageView)
        view.addSubview(descriptionLabel)
    }
    
    private func setUpLayout() {
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            imageView.widthAnchor.constraint(equalToConstant: 200),
            imageView.heightAnchor.constraint(equalToConstant: 200)
        ])
        
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 20),
            descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
        ])
    }
    
    private func setUpBinding() {
        if let viewModel = viewModel {
            viewModel.details.addAndNotify(observer: self) { [weak self] details in
                self?.displayData()

            }
        }
    }
    
    private func displayData() {
        if let image = viewModel?.details.value.image {
            guard let url = URL(string: image) else { return }
            self.imageView.kf.setImage(with: url)
            self.descriptionLabel.text = viewModel?.details.value.text ?? ""
        }
    }
}
