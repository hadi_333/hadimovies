//
//  DetailsModel.swift
//  HadiMovies
//
//  Created by isima on 19/09/2023.
//

import Foundation

// MARK: - DetailsModel

struct DetailsModel {
    let image: String
    let text: String
    
    init(detailsEntity: DetailsEntity) {
        self.image = Constants.imagePrefixe + (detailsEntity.posterPath ?? "") 
        self.text = detailsEntity.overview ?? ""
    }
    
    init() {
        self.image = ""
        self.text = ""
    }
}
