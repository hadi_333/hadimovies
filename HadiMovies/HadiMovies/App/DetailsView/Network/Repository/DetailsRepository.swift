//
//  DetailsRepository.swift
//  HadiMovies
//
//  Created by isima on 19/09/2023.
//

import Foundation

protocol DetailsRepository {
    func fetchDetails(id: String, _ completion: @escaping ((DetailsModel) -> Void))
}

final class DetailsRepositoryImpl: DetailsRepository {
    
    let apiClient: APIClientProtocol
    
    init(apiClient: APIClientProtocol = APIClient()) {
        self.apiClient = apiClient
    }
    
    func fetchDetails(id: String, _ completion: @escaping ((DetailsModel) -> Void)) {
        apiClient.send(GetDetails(id: id)) { result in
            switch result {
            case .success(let data):
                let movie = DetailsMapper.detailEntityToDetailsModel(detailsEntity: data)
                completion(movie)
            case .failure(let error):
                debugPrint(error.localizedDescription)
            }
        }
    }
    
}

