//
//  DetailsMapper.swift
//  HadiMovies
//
//  Created by isima on 19/09/2023.
//

import Foundation

final class DetailsMapper {
    
    static func detailEntityToDetailsModel(detailsEntity: DetailsEntity) -> DetailsModel {
        return DetailsModel(detailsEntity: detailsEntity)
    }
}
