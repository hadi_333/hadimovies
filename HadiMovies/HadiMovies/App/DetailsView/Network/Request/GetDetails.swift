//
//  GetDetails.swift
//  HadiMovies
//
//  Created by isima on 19/09/2023.
//

import Foundation

struct GetDetails: APIRequest {
    typealias Response = DetailsEntity
    
    var resourceName: String {
        let enviroment = Plist.getEnvironment() ?? ""
        if enviroment.contains("PreProd") {
            return "/" + Constants.apiVersionEarlier + "/movie/\(id)"
        } else {
            return "/" + Constants.apiVersionLatest + "/movie/\(id)"
        }
    }
    
    private let language: String
    private let id: String
    
    init(language: String = "en-US", id: String) {
        self.language = language
        self.id = id
    }
}
