//
//  Plist.swift
//  HadiMovies
//
//  Created by Hadi KUDSI on 18/09/2023.
//

import Foundation

struct Plist {

    static func getUrlFromPlist() -> String? {
        guard let path = Bundle.main.path(forResource: "HadiMovies", ofType: "plist") else { return "fatal Error" }
        let url = URL(fileURLWithPath: path)
        let contents = NSDictionary(contentsOf: url) as? [String: String] ?? [:]
        return contents["url"]
    }
    
    static func getEnvironment() -> String? {
        let currentConfiguration = Bundle.main.object(forInfoDictionaryKey: "Configuration") as! String
        return currentConfiguration
    }
}
