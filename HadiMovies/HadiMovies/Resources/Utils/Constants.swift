//
//  Constants.swift
//  HadiMovies
//
//  Created by Hadi KUDSI on 18/09/2023.
//

import Foundation

struct Constants {
    static let apiKey = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI2NDg5Y2Y0MTQzNDU5MGNjMDgzNjg5NmJiZTY0OWM1MSIsInN1YiI6IjY1MDgwMjgyM2NkMTJjMDBjYTU2NDYwYyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.SE-qlUipiMG9bsHsUA1ydf-eXlJeSiSVTnQesxHKqbY"
    
    static let apiVersionLatest = "3"
    
    static let apiVersionEarlier = "2"
    
    static let homepageTypes = ["Now Playing", "Popular", "Top Rated", "Upcoming"]
    
    static let imagePrefixe = "https://image.tmdb.org/t/p/w500"
}
