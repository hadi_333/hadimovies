//
//  DynamicValue.swift
//  HadiMovies
//
//  Created by isima on 19/09/2023.
//

import Foundation

/// Dynamic generic type used for observation
class DynamicValue<T> {
    var value: T {
        didSet {
            self.notify()
        }
    }
    
    typealias CompletionHandler = ((T) -> Void)
    
    private var observers = [String: CompletionHandler]()
    
    init(_ value: T) {
        self.value = value
    }
    
    /**
     Add observer and handle a completion.
     - parameters:
        - observer: The observer
        - completionHandler: The completion handler
     */
    public func addObserver(_ observer: NSObject, completionHandler: @escaping CompletionHandler) {
        observers[observer.description] = completionHandler
    }
    
    /**
     Add observer and notify directly with handle a completion.
     - parameters:
        - observer: The observer
        - completionHandler: The completion handler
     
     Do same thing that addObserver, add a notification just after observation.
     */
    public func addAndNotify(observer: NSObject, completionHandler: @escaping CompletionHandler) {
        self.addObserver(observer, completionHandler: completionHandler)
        self.notify()
    }
    
    private func notify() {
        observers.forEach({ $0.value(value) })
    }
    
    deinit {
        observers.removeAll()
    }
}
