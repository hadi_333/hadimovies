//
//  MovieViewModelTest.swift
//  HadiMoviesTests
//
//  Created by isima on 19/09/2023.
//

import XCTest
@testable import HadiMovies

final class MovieViewModelTest: XCTestCase {
    var viewModel: MoviesViewModelImpl!
    var mockRepository: MoviesRepositoryMock!
    let aMovie = MovieModel(movieEntity: MovieEntity(
        adult: false,
        backdropPath: "/ctMserH8g2SeOAnCw5gFjdQF8mo.jpg",
        genreIds: nil,
        id: 346698,
        originalLanguage: nil,
        originalTitle: nil,
        overview: nil,
        popularity: 5.5,
        posterPath: "/ctMserH8g2SeOAnCw5gFjdQF8mo.jpg",
        releaseDate: "2023-12-12",
        title: "Barbie",
        video: nil,
        voteAverage: 7.3,
        voteCount: nil))
        
    override func setUp() {
        super.setUp()
        mockRepository = MoviesRepositoryMock()
        viewModel = MoviesViewModelImpl(moviesRepository: mockRepository)
    }
    
    override func tearDown() {
        viewModel = nil
        mockRepository = nil
        super.tearDown()
    }
    
    func testFetchPopularMovies() {
        let expectedMovies = [aMovie]
        mockRepository.popularMovies = expectedMovies
        
        viewModel.fetchPopularMovies()
        
        XCTAssertEqual(viewModel.popularMovies.value.count, expectedMovies.count)
        XCTAssertEqual(viewModel.popularMovies.value.first?.id, expectedMovies.first?.id)
    }
    func testFetchTopRatedMovies() {
        let expectedMovies = [aMovie]
        mockRepository.topRatedMovies = expectedMovies
        
        viewModel.fetchTopRatedMovies()
        
        XCTAssertEqual(viewModel.topRatedMovies.value.count, expectedMovies.count)
        XCTAssertEqual(viewModel.topRatedMovies.value.first?.id, expectedMovies.first?.id)
    }
    
    func testFetchNowPlayingMovies() {
        let expectedMovies = [aMovie]
        mockRepository.nowPlayingMovies = expectedMovies
        
        viewModel.fetchNowPlayingMovies()
        
        XCTAssertEqual(viewModel.nowPlayingMovies.value.count, expectedMovies.count)
        XCTAssertEqual(viewModel.nowPlayingMovies.value.first?.id, expectedMovies.first?.id)
    }
    
    func testFetchUpcomingMovies() {
        let expectedMovies = [aMovie]
        mockRepository.upcomingMovies = expectedMovies
        
        viewModel.fetchUpcomingMovies()
        
        XCTAssertEqual(viewModel.UpcomingMovies.value.count, expectedMovies.count)
        XCTAssertEqual(viewModel.UpcomingMovies.value.first?.id, expectedMovies.first?.id)
    }
    

    func testFetchHomePage() {
        
        let expectedPopularMovies = [aMovie]
        let expectedNowPlayingMovies = [aMovie]
        let expectedTopRatedMovies = [aMovie]
        let expectedUpcomingMovies = [aMovie]

        mockRepository.popularMovies = expectedPopularMovies
        mockRepository.nowPlayingMovies = expectedNowPlayingMovies
        mockRepository.topRatedMovies = expectedTopRatedMovies
        mockRepository.upcomingMovies = expectedUpcomingMovies

        viewModel.fetchHomePage()

        XCTAssertEqual(viewModel.popularMovies.value.count, expectedPopularMovies.count)
        XCTAssertEqual(viewModel.nowPlayingMovies.value.count, expectedNowPlayingMovies.count)
        XCTAssertEqual(viewModel.topRatedMovies.value.count, expectedTopRatedMovies.count)
        XCTAssertEqual(viewModel.UpcomingMovies.value.count, expectedUpcomingMovies.count)
    }

}
