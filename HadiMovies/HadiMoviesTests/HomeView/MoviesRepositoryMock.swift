//
//  MoviesRepositoryMock.swift
//  HadiMoviesTests
//
//  Created by isima on 19/09/2023.
//

import Foundation
@testable import HadiMovies

class MoviesRepositoryMock: MoviesRepository {
    var popularMovies: [MovieModel]?
    var nowPlayingMovies: [MovieModel]?
    var topRatedMovies: [MovieModel]?
    var upcomingMovies: [MovieModel]?
    
    func getPopularMovies(_ completion: @escaping (([MovieModel]) -> Void)) {
        if let movies = popularMovies {
            completion(movies)
        }
    }
    
    func getNowPlayingMovies(_ completion: @escaping (([MovieModel]) -> Void)) {
        if let movies = nowPlayingMovies {
            completion(movies)
        }
    }
    
    func getTopRatedMovies(_ completion: @escaping (([MovieModel]) -> Void)) {
        if let movies = topRatedMovies {
            completion(movies)
        }
    }
    
    func getUpcomingMovies(_ completion: @escaping (([MovieModel]) -> Void)) {
        if let movies = upcomingMovies {
            completion(movies)
        }
    }
}
