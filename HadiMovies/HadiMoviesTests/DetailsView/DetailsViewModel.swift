//
//  DetailsViewModel.swift
//  HadiMoviesTests
//
//  Created by isima on 19/09/2023.
//

import XCTest
@testable import HadiMovies

final class DetailsViewModel: XCTestCase {
    var viewModel: DetailsViewModelImpl!
    var mockRepository: DetailsRepositoryMock!
    let aDetails = DetailsModel(detailsEntity: DetailsEntity(
        adult: nil,
        backdropPath: nil,
        budget: nil,
        homepage: nil,
        id: 1101,
        imdbID: nil,
        originalLanguage: nil,
        originalTitle: nil,
        overview: "When a group of friends discover how to conjure spirits using an embalmed hand, they become hooked on the new thrill, until one of them goes too far and unleashes terrifying supernatural forces.",
        popularity: nil,
        posterPath: Constants.imagePrefixe + "/kdPMUMJzyYAc4roD52qavX0nLIC.jpg",
        releaseDate: nil,
        revenue: nil,
        runtime: nil,
        status: nil,
        tagline: nil,
        title: nil,
        video: nil,
        voteAverage: nil,
        voteCount: nil))
    
    override func setUp() {
        super.setUp()
        mockRepository = DetailsRepositoryMock()
        viewModel = DetailsViewModelImpl(detailsRepository: mockRepository, id: 0)
    }
    
    override func tearDown() {
        viewModel = nil
        mockRepository = nil
        super.tearDown()
    }
    
    func testFetchDetails() {
        let expectedDetails = aDetails
        mockRepository.details = expectedDetails
        
        viewModel.fetchDetails()
        
        XCTAssertEqual(viewModel.details.value.image, expectedDetails.image)

    }
    
    func testFetchDetailsCallsRepository() {
        viewModel.fetchDetails()
        XCTAssertTrue(mockRepository.fetchDetailsCalled)
    }
}
