//
//  DetailsRepositoryMock.swift
//  HadiMoviesTests
//
//  Created by isima on 19/09/2023.
//

import Foundation
@testable import HadiMovies

class DetailsRepositoryMock: DetailsRepository {
    var fetchDetailsCalled = false
    var details: DetailsModel?
     
    func fetchDetails(id: String, _ completion: @escaping ((HadiMovies.DetailsModel) -> Void)) {
        fetchDetailsCalled = true
        if let details = details {
            completion(details)
        }
    }
}
